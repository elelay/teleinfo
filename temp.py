#!/usr/bin/env python3
# encoding: utf-8

import argparse
import logging
import os
import re
import sys
import time
from datetime import datetime
from urllib.parse import urlparse

from influxdb import InfluxDBClient

from teleinfo import Saver

logger = logging.getLogger(__name__)


W1_TEMP_RE = re.compile(
    r"""(?:[\da-z]{2} ){9}: crc=[\da-z]+ (YES|NO)\n(?:[\da-z]{2} ){9}t=(-?\d+)""",
    re.MULTILINE,
)


def main():
    parser = argparse.ArgumentParser(description="1-Wire temperature reader")
    parser.add_argument("--debug", "-d", action="store_true")
    parser.add_argument("--influxdb", type=str, help="url to influxdb")
    parser.add_argument("--ssl", action="store_true")
    parser.add_argument(
        "--save-dir", default=".", help="directory to save to if necessary"
    )
    parser.add_argument(
        "--name",
        default="temp",
        help="influx collection and saver name",
    )
    parser.add_argument(
        "source",
        default="/sys/bus/w1/devices/10-000801714bf3/w1_slave",
        help="path to 1-wire device w1_slave pseudo-file",
    )

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    if not os.path.exists(args.source):
        logger.error("Source %s doesn't exist", args.source)
        sys.exit(-1)

    if args.influxdb:
        o = urlparse(args.influxdb)
        ssl = bool(args.ssl)
        verify_ssl = ssl
        database = (o.path or "/")[1:]
        if "/" in database:
            path, database = database.split("/")
        else:
            path = ""
        influx = InfluxDBClient(
            host=o.hostname,
            port=o.port,
            username=o.username,
            password=o.password,
            path=path,
            database=database,
            pool_size=1,
            ssl=ssl,
            verify_ssl=verify_ssl,
        )
    else:
        influx = None

    saver = Saver(influx, save_dir=args.save_dir, name=args.name, save_period=2)
    saver.send_disk_points(force=True)

    while True:
        with open(args.source, "r") as f:
            data = f.read()
        m = W1_TEMP_RE.match(data)
        temp = None
        if m:
            if m.group(1) == "YES":
                temp = round(int(m.group(2)) / 1000, 1)
            else:
                logger.warning("temp crc failure")
        else:
            logger.warning('temp regex not match: "%s"', data)
        if temp:
            dat = datetime.utcnow().isoformat()
            # print("%s %f" % (dat, temp))
            parsed = dict(ext_garage=temp)
            saver.add_point({"measurement": args.name, "time": dat, "fields": parsed})
        time.sleep(30)


if __name__ == "__main__":
    main()
