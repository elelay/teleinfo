# teleinfo

Read Teleinfo from Linky counter

See https://www.enedis.fr/sites/default/files/Enedis-NOI-CPT_54E.pdf for documentation on the Linky smart counter.

# Setup

## Influxdb in docker

	mkdir influxdata
	docker run -d --name shome-influx -p8086:8086 \
		-v $(pwd)/influxdata:/var/lib/influxdb \
		-e INFLUXDB_DB=elec \
		-e INFLUXDB_USER=tinfo -e INFLUXDB_USER_PASSWORD=tinfo \
		influxdb:1.7.10

## Dependencies

`teleinfo.py` depends on:

 - python3
 - pyserial
 - influxdb

Install:

	virtualenv VENV
	. VENV/bin/activate
	pip install -r requirements.txt

# Usage

## Standard usage

	./teleinfo.py --influxdb influx://tinfo:tinfo@localhost:8086/elec --save-dir /tmp /dev/ttyAMA0

## Dev setup

Export serial device On Rpi:

	socat file:/dev/ttyAMA0,raw,echo=0,b9600 tcp-l:3016,fork


Connect using NetworkReader:

	./teleinfo.py --influxdb influx://tinfo:tinfo@localhost:8086/elec --save-dir /tmp tcp://rpi.local:3016


Offline (no influxdb) mode (will eat your disk up because it saves as json):

	./teleinfo.py --save-dir /mnt/usbkey /dev/ttyAMA0

# Dev commands

`make beauty lint` to beautify and lint the code.


# Temperatures

`temp.py` reads DS1820 temp sensors and sends values to influxdb

# RPi setup

On a RPi 1, running [archlinux arm](https://archlinuxarm.org/platforms/armv6/raspberry-pi), do the system install.

Install picocom for testing. Install python-influxdb and python-pyserial (for temp.py) to skip a virtualenv.

	$ pacman -S python3 python-pyserial python-influxdb

## Teleinfo

Edit /boot/cmdline.txt to remove references to ttyAMA0 (serial console).

	$ cat /boot/cmdline.txt
	root=/dev/mmcblk0p2 rw rootwait console=tty1 selinux=0 plymouth.enable=0 smsc95xx.turbo_mode=N dwc_otg.lpm_enable=0 elevator=noop

Disable allocating a console on the port

	$ systemctl disable serial-getty@ttyAMA0.service

Test using picocom (Ctrl-A Ctrl-Q to exit)

	$ picocom -b 9600 -d 7 -p e -f n /dev/ttyAMA0
	Ctrl-A Ctrl-Q


# Temperatures

Source: [framboise314](https://www.framboise314.fr/mesure-de-temperature-1-wire-ds18b20-avec-le-raspberry-pi/#Configuration_du_Raspberry_Pi)
(French, read edits in blue at the end of the article).

Add the w1-gpio overlay with the GPIO pin 22 in my case (PyTInfo already uses GPIO4):

	$ cat /boot/config.txt
	# See /boot/overlays/README for all available options
	gpu_mem=64
	initramfs initramfs-linux.img followkernel
	dtoverlay=w1-gpio,gpiopin=22

List devices

	$ ls /sys/bus/w1/devices/
	10-0008016ffea6  10-000801714bf3  w1_bus_master1

Read temperature

	$ cat /sys/bus/w1/devices/10-0008016ffea6/w1_slave
	0f 00 4b 46 ff ff 07 10 c8 : crc=c8 YES
	0f 00 4b 46 ff ff 07 10 c8 t=7312
