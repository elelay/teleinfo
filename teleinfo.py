#!/usr/bin/env python3
# encoding: utf-8

import argparse
import datetime
import glob
import json
import logging
import os
import re
import socket
import time
from urllib.parse import urlparse

import serial
from influxdb import InfluxDBClient

logger = logging.getLogger(__name__)


class Reader:
    """
        read data from somewhere.
        usable as context manager
    """

    def open(self):
        """ open the data source """
        raise NotImplementedError()

    def close(self):
        """ close the data source """
        raise NotImplementedError()

    def readline(self):
        """
            read a full line
            :return bytes: read line (with eol) or None
        """
        raise NotImplementedError()

    def readone(self):
        """
            read a single byte
            :return bytes: read byte (as 1-length bytes) or None
        """
        raise NotImplementedError()

    def __enter__(self):
        """ context manager """
        raise NotImplementedError()

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ context manager """
        raise NotImplementedError()


class SerialReader(Reader):
    """
        read data from serial port.
        usable as context manager
    """

    def __init__(self, path):
        """
        :param str path: path to serial
        """
        self._t = None
        self._path = path

    def open(self):
        """ open the serial port """
        self._t = serial.Serial(
            self._path,
            baudrate=9600,
            bytesize=serial.SEVENBITS,
            parity=serial.PARITY_EVEN,
        )

    def close(self):
        """ close the serial port """
        if self._t:
            self._t.close()
            self._t = None

    def readline(self):
        """
            read a full line from serial port
            :return bytes: read line (with eol) or None
        """
        if self._t:
            return self._t.readline()
        return None

    def readone(self):
        """
            read a single byte from serial port
            :return bytes: read byte (as 1-length bytes) or None
        """
        if self._t:
            return self._t.read() or None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False


class NetworkReader(Reader):
    """
        read data from tcp.
        usable as context manager.
        in development, export the serial port using socat:
        socat file:/dev/ttyAMA0,raw,echo=0,b9600 tcp-l:3016,fork
    """

    def __init__(self, host, port):
        self._host = host
        self._port = port
        self._s = None
        self._s_file = None

    def open(self):
        """ open the socket """
        if self._s:
            raise Exception("Already open")
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.connect((self._host, self._port))
        self._s_file = self._s.makefile("rb", buffering=0)

    def close(self):
        """ close the socket """
        if self._s:
            self._s.close()
            del self._s
            del self._s_file

    def readline(self):
        """
            read a full line from the socket
            :return bytes: read line (with eol) or None
        """
        if self._s_file:
            return self._s_file.readline()
        return None

    def readone(self):
        """
            read a single byte from the socket
            :return bytes: read byte (as 1-length bytes) or None
        """
        if self._s_file:
            return self._s_file.read(1)

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False


class TInfoData:
    """Teleinfo data item description, see table in Enedis note page 18 and later """

    __slots__ = (
        "description",
        "key",
        "tsed",
        "length",
        "unit",
        "tri_only",
        "prod_only",
    )

    def __init__(
        self,
        description,
        key,
        length,
        unit="",
        tsed=False,
        tri_only=False,
        prod_only=False,
    ):
        """
        :param str description: short field description
        :param str key: key used in the teleinfo frame
        :param int length: field length
        :param str unit: unit if applicable or ''
        :param bool tsed: is this data item timestamped
        :param bool tri_only: is this data only found for three-phase
        :param bool prod_only: is this data only found for energy producers
        """
        self.key = key
        self.description = description
        self.length = length
        self.unit = unit
        self.tsed = tsed
        self.tri_only = tri_only
        self.prod_only = prod_only

    def __repr__(self):
        return dict(**self)


# see table in Enedis note page 18 and later
TICV2 = [
    TInfoData("Adresse Secondaire du Compteur", "ADSC", 12),
    TInfoData("Version de la TIC", "VTIC", 2),
    TInfoData("Date et heure courante", "DATE", 0, tsed=True),
    TInfoData("Nom du calendrier tarifaire fournisseur", "NGTF", 16),
    TInfoData("Libellé tarif fournisseur en cours", "LTARF", 16),
    TInfoData("Energie active soutirée totale", "EAST", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 01", "EASF01", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 02", "EASF02", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 03", "EASF03", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 04", "EASF04", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 05", "EASF05", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 06", "EASF06", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 07", "EASF07", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 08", "EASF08", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 09", "EASF09", 9, unit="Wh"),
    TInfoData("Energie active soutirée fournisseur, index 10", "EASF10", 9, unit="Wh"),
    TInfoData("Energie active soutirée distributeur, index 01", "EASD01", 9, unit="Wh"),
    TInfoData("Energie active soutirée distributeur, index 02", "EASD02", 9, unit="Wh"),
    TInfoData("Energie active soutirée distributeur, index 03", "EASD03", 9, unit="Wh"),
    TInfoData("Energie active soutirée distributeur, index 04", "EASD04", 9, unit="Wh"),
    TInfoData("Energie active injectée totale", "EAIT", 9, unit="Wh", prod_only=True),
    TInfoData("Energie réactive Q1 totale", "ERQ1", 9, unit="Wh", prod_only=True),
    TInfoData("Energie réactive Q2 totale", "ERQ2", 9, unit="Wh", prod_only=True),
    TInfoData("Energie réactive Q3 totale", "ERQ3", 9, unit="Wh", prod_only=True),
    TInfoData("Energie réactive Q4 totale", "ERQ4", 9, unit="Wh", prod_only=True),
    TInfoData("Courant efficace, phase 1", "IRMS1", 3, unit="A"),
    TInfoData("Courant efficace, phase 2", "IRMS2", 3, unit="A", tri_only=True),
    TInfoData("Courant efficace, phase 3", "IRMS3", 3, unit="A", tri_only=True),
    TInfoData("Tension efficace, phase 1", "URMS1", 3, unit="V"),
    TInfoData("Tension efficace, phase 2", "URMS2", 3, unit="V", tri_only=True),
    TInfoData("Tension efficace, phase 3", "URMS3", 3, unit="V", tri_only=True),
    TInfoData("Puissance app. de référence", "PREF", 2, unit="kVA"),
    TInfoData("Puissance app. de coupure", "PCOUP", 2, unit="kVA"),
    TInfoData("Puissance app. instantanée soutirée", "SINSTS", 5, unit="VA"),
    TInfoData(
        "Puissance app. instantanée soutirée, phase 1",
        "SINSTS1",
        5,
        unit="VA",
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. instantanée soutirée, phase 2",
        "SINSTS2",
        5,
        unit="VA",
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. instantanée soutirée, phase 3",
        "SINSTS3",
        5,
        unit="VA",
        tri_only=True,
    ),
    TInfoData("Puissance app. max soutirée n", "SMAXN", 5, unit="VA", tsed=True),
    TInfoData(
        "Puissance app. max soutirée n, phase 1",
        "SMAXN1",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. max soutirée n, phase 2",
        "SMAXN2",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. max soutirée n, phase 3",
        "SMAXN3",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData("Puissance app. max soutirée n-1", "SMAXN-1", 5, unit="VA", tsed=True),
    TInfoData(
        "Puissance app. max soutirée n-1, phase 1",
        "SMAXN1-1",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. max soutirée n-1, phase 2",
        "SMAXN2-1",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. max soutirée n-1, phase 3",
        "SMAXN3-1",
        5,
        unit="VA",
        tsed=True,
        tri_only=True,
    ),
    TInfoData(
        "Puissance app. instantanée injectée", "SINSTI", 5, unit="VA", prod_only=True
    ),
    TInfoData(
        "Puissance app. max injectée n",
        "SMAXIN",
        5,
        unit="VA",
        tsed=True,
        prod_only=True,
    ),
    TInfoData(
        "Puissance app. max injectée n-1",
        "SMAXIN-1",
        5,
        unit="VA",
        tsed=True,
        prod_only=True,
    ),
    TInfoData(
        "Point n de la courbe de charge active soutirée",
        "CCASN",
        5,
        unit="W",
        tsed=True,
    ),
    TInfoData(
        "Point n-1 de la courbe de charge active soutirée",
        "CCASN-1",
        5,
        unit="W",
        tsed=True,
    ),
    TInfoData(
        "Point n de la courbe de charge active injectée",
        "CCAIN",
        5,
        unit="W",
        tsed=True,
        prod_only=True,
    ),
    TInfoData(
        "Point n-1 de la courbe de charge active injectée",
        "CCAIN-1",
        5,
        unit="W",
        tsed=True,
        prod_only=True,
    ),
    TInfoData("Tension moyenne phase 1", "UMOY1", 3, unit="V", tsed=True),
    TInfoData(
        "Tension moyenne phase 2", "UMOY2", 3, unit="V", tsed=True, tri_only=True
    ),
    TInfoData(
        "Tension moyenne phase 3", "UMOY3", 3, unit="V", tsed=True, tri_only=True
    ),
    TInfoData("Registre de Statuts", "STGE", 8),
    TInfoData("Début Pointe Mobile 1", "DPM1", 2, tsed=True),
    TInfoData("Fin Pointe Mobile 1", "FPM1", 2, tsed=True),
    TInfoData("Début Pointe Mobile 2", "DPM2", 2, tsed=True),
    TInfoData("Fin Pointe Mobile 2", "FPM2", 2, tsed=True),
    TInfoData("Début Pointe Mobile 3", "DPM3", 2, tsed=True),
    TInfoData("Fin Pointe Mobile 3", "FPM3", 2, tsed=True),
    TInfoData("Message court", "MSG1", 32),
    TInfoData("Message ultra court", "MSG2", 16),
    TInfoData("PRM", "PRM", 14),
    TInfoData("Relais", "RELAIS", 3),
    TInfoData("Numéro de l'index tarifaire en cours", "NTARF", 2),
    TInfoData("Numéro du jour en cours calendrier fournisseur", "NJOURF", 2),
    TInfoData("Numéro du prochain jour calendrier fournisseur", "NJOURF+1", 2),
    TInfoData("Profil du prochain jour calendrier fournisseur", "PJOURF+1", 98),
    TInfoData("Profil du prochain jour de pointe", "PPOINTE", 98),
]

# what interests me to see plotted
PLOTTED = set(("EAST", "SINSTS", "URMS1", "IRMS1"))

# self-test
assert len(set(d.key for d in TICV2)) == len(TICV2)
assert len(set(d.description for d in TICV2)) == len(TICV2)


class TInfo:
    """
    read and parse frames from a teleinfo Reader
    """

    GROUP_RE = re.compile(
        rb"""
        (?P<checksumed>  # checksum covers everything up to checksum
            (?P<label>[^\t]{1,8})\t  # data label
            (?:(?P<ts>[HEhe ]\d{12})\t)?  # optional timestamp
            (?P<data>[\x20-\x7E]*)\t)  # data
            (?P<cs>.)  # single byte checksum
        """,
        re.X,
    )

    def __init__(self, t):
        """
        :param Reader t: data source
        """
        self._t = t

    def _sync(self):
        """
        skip until reaching the frame begining
        :return bool: False if aborted
        """
        r = b"c"
        while r != b"\x02" and r is not None:
            r = self._t.readone()
        return bool(r)

    def _read_raw_lines(self):
        """
        read a full frame (should have synced before)
        :return list(bytes): raw lines, without begining or end markers
        """
        buf = b""
        r = self._t.readone()
        while r is not None and r != b"\x03":
            buf += r
            r = self._t.readone()
        if r is None:
            return None
        if not buf:
            return []
        if buf[0] != 0x0A:
            return None
        if buf[-1] != 0x0D:
            return None
        return buf[1:-1].split(b"\r\n")

    def read_frame(self):
        """
        sync and read frame
        :return list(dict(str, str)): data as dict(label, ts, data) list
        """
        if not self._sync():
            return None
        frame = {}
        lines = self._read_raw_lines()
        if lines is None:
            return None
        # print(lines)
        for l in lines:
            m = self.GROUP_RE.match(l)
            if not m:
                return None
            label = m.group("label")
            ts = m.group("ts")
            data = m.group("data")
            cs = m.group("cs")[0]
            comp_cs = sum(m.group("checksumed"))
            trunc = (comp_cs & 0x3F) + 0x20
            if trunc != cs:
                logger.error(
                    "invalid CS l=%r, comp_cs=%r, trunc=%r, cs=%r"
                    % (l, comp_cs, trunc, cs)
                )
                return None
            d = dict(ts=(ts or b"").decode(), data=data.decode())
            label = label.decode()
            # print(label, d)
            frame[label] = d
        return frame

    def parse_frame(self, frame):
        """ parse a frame to be injected to Saver
        :param dict(str, str) frame: frame returned by read_frame()
        :return dict(str, str): data to be plotted (DATE is the time key)
        """
        if not frame:
            return None
        if "VTIC" not in frame:
            raise Exception("Missing VTIC")
        if frame["VTIC"]["data"] != "02":
            raise Exception("Only supports TIC V2, not %r" % frame["VTIC"])
        frame_data = {}
        for ti in TICV2:
            if ti.key in frame:
                d = frame[ti.key]
                if ti.tsed:
                    ts = self.ts_to_datetime(d["ts"])
                else:
                    ts = ""
                if len(d["data"]) != ti.length:
                    raise Exception(
                        "Invalid length %s=%s expected %i"
                        % (ti.key, len(d["data"]), ti.length)
                    )
                logger.debug(
                    "%s\t%s%s %s",
                    ti.description,
                    d["data"],
                    ti.unit if ti.unit else "",
                    ts,
                )
                if ti.key == "DATE":
                    frame_data[ti.key] = ts
                if ti.key in PLOTTED:
                    # TODO: if want to save string data...
                    frame_data[ti.key] = int(d["data"].lstrip("0") or "0")
        if "DATE" not in frame_data:
            logger.warning("no DATE in frame, dropping %r", frame_data)
            frame_data = {}
        return frame_data

    @staticmethod
    def ts_to_datetime(ts):
        """
        teleinfo timestamp to datetime
        :param str ts: timestamp in teleinfo format (Enedis note p.17)
        """
        d = time.strptime(ts[1:], "%y%m%d%H%M%S")
        delta = 2 if ts[0] in ("e", "E") else 1
        tz = datetime.timezone(datetime.timedelta(hours=delta), name="")
        dat = datetime.datetime(
            d.tm_year, d.tm_mon, d.tm_mday, d.tm_hour, d.tm_min, d.tm_sec, tzinfo=tz
        )
        # print(d, '=>', dat)
        return dat

    def __enter__(self):
        """ context-manager implem """
        self._t.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ context-manager implem """
        self._t.close()
        return False


class Saver:
    """ save read points to influxdb or disk """

    def __init__(self, influx=None, save_dir=".", name="saved", save_period=30):
        """
        :param InfluxDBClient influx: influxdb client (None to only save to disk)
        :param str save_dir: directory to save to if influx not available (defaults to .)
        """
        self.influx = influx
        """ influxdb client """
        self.save_dir = save_dir
        """ directory to save to """
        self.points = []
        """ points in memory """
        self.ondisk = False
        """ known to have saved points on disk """
        self.offline = 0
        """ known offline (failed ping) """
        self.name = name
        """ name to use for on-disk saves """
        self.save_period = save_period
        """ how often to save / push to influx datapoints """

    def add_point(self, point):
        """
        add a point, maybe triggering sending to influxdb
        :param dict point: point in influx client compatible format (time, measurement, fields)
        """
        self.points.append(point)
        if len(self.points) >= self.save_period:
            if not self.influx:
                return self._save_points()
            if not self.offline or self.offline > 5:
                self.offline = 0
                try:
                    self.influx.ping()
                except Exception as e:
                    logger.warning("influx not reachable: %s", e)
                    self.offline = 1
                    self._save_points()
                else:
                    try:
                        self.send_disk_points()  # resume saved points
                        self.influx.write_points(self.points, time_precision="s")
                        logger.info("saved %i points", len(self.points))
                        self.points = []
                    except Exception as e:
                        logger.error("writing to influx (%s), saving...", e)
                        self._save_points()
            else:
                self.offline += 1

    def _save_points(self):
        """ potentially save points to disk """
        if len(self.points) >= 4 * self.save_period:
            with open(
                "%s%i.json" % (self.name, time.time()), "w", encoding="utf-8"
            ) as f:
                json.dump(self.points, f)
            self.points = []
            self.ondisk = True

    def send_disk_points(self, force=False):
        """ load points from disk and send them """
        if not self.influx:
            return
        try:
            self.influx.ping()
            if force or self.ondisk:
                for p in sorted(glob.glob(os.path.join(self.save_dir, "%s*.json" % self.name))):
                    with open(p, "r", encoding="utf-8") as f:
                        try:
                            points = json.load(f)
                            logger.debug(
                                "sending %i on-disk points from %s", len(points), p
                            )
                            self.influx.write_points(points, time_precision="s")
                            logger.info("sent on disk points, cleaning %s", p)
                            os.remove(p)
                        except json.JSONDecodeError as e:
                            logger.warning('invalid saved json %s: %s', p, e, exc_info=True)
                        except Exception as e:
                            logger.error("E: sending disk points %s: %s", p, e, exc_info=True)
        except Exception as e:
            logger.error("E: sending disk points: %s", e)


def main():
    parser = argparse.ArgumentParser(description="Teleinfo processor")
    parser.add_argument("--debug", "-d", action="store_true")
    parser.add_argument("--influxdb", type=str, help="url to influxdb")
    parser.add_argument("--ssl", action="store_true")
    parser.add_argument(
        "--save-dir", default=".", help="directory to save to if necessary"
    )
    parser.add_argument(
        "source", default="/dev/ttyAMA0", help="path to serial port or tcp://host:port"
    )

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    o = urlparse(args.source)
    if o.scheme == "tcp":
        reader = NetworkReader(o.hostname, o.port)
    else:
        reader = SerialReader(o.path)

    if args.influxdb:
        o = urlparse(args.influxdb)
        ssl = bool(args.ssl)
        verify_ssl = ssl
        database = (o.path or "/")[1:]
        if "/" in database:
            path, database = database.split("/")
        else:
            path = ""
        influx = InfluxDBClient(
            host=o.hostname,
            port=o.port,
            username=o.username,
            password=o.password,
            path=path,
            database=database,
            pool_size=1,
            ssl=ssl,
            verify_ssl=verify_ssl,
        )
    else:
        influx = None

    saver = Saver(influx, save_dir=args.save_dir)
    saver.send_disk_points(force=True)
    with TInfo(reader) as tinfo:
        while True:
            data = tinfo.read_frame()
            # print(data)
            parsed = tinfo.parse_frame(data)
            # print(parsed)
            # print("____________________________")
            if parsed:
                dat = parsed["DATE"].isoformat()
                del parsed["DATE"]
                saver.add_point(
                    {"measurement": "teleinfo", "time": dat, "fields": parsed}
                )


if __name__ == "__main__":
    main()
