SOURCES:=teleinfo.py temp.py

lint: $(SOURCES)
	flake8 $^

beauty: $(SOURCES)
	black $^
	isort $^
